#include "Biblioteka.h"

using namespace std;

ALLEGRO_BITMAP *ikonka1(int w,int h)   {
    ALLEGRO_BITMAP *icon=al_create_bitmap(w,h);
    ALLEGRO_DISPLAY *current_display = al_get_current_display();
    al_set_target_bitmap(icon);
    al_clear_to_color(al_map_rgb(255, 0, 0));
    al_draw_line( 0, 0,w,h,al_map_rgb(0, 255, 0) , 1);
    al_draw_line( w, 0,0,h,al_map_rgb(0, 255, 0) , 1);
    al_set_target_backbuffer(current_display);
    return icon;
    }

ALLEGRO_BITMAP *kolo(int r) {
    int w=2*r;
    int h=2*r;
    ALLEGRO_BITMAP *icon=al_create_bitmap(w,h);
    ALLEGRO_DISPLAY *current_display = al_get_current_display();
    al_set_target_bitmap(icon);
    al_draw_filled_circle(w/2,w/2,r,al_map_rgb(255,0,0));
    al_set_target_backbuffer(current_display);
    return icon;
    }

ALLEGRO_BITMAP *przycisk_tekst(int w, int h, string tekst)  {
    ALLEGRO_FONT *font = al_load_ttf_font("Tungsten-Bold.ttf", 30, 0);
    ALLEGRO_BITMAP *icon=al_create_bitmap(w,h);
    ALLEGRO_DISPLAY *current_display = al_get_current_display();
    al_set_target_bitmap(icon);
    al_clear_to_color(al_map_rgb(0,0,255));
    al_draw_textf(font,al_map_rgb(255,255,0),w/2,h/4,1,"%s",tekst.c_str());
    al_set_target_backbuffer(current_display);
    return icon;
    }

ALLEGRO_BITMAP *troj_sierp(int w,int h) {
    ALLEGRO_BITMAP *icon=al_create_bitmap(w,h);
    ALLEGRO_DISPLAY *current_display = al_get_current_display();
    al_set_target_bitmap(icon);
    rysuj_troj_sierp(0,0,640,0,320,480,3);
    al_set_target_backbuffer(current_display);
    return icon;
    }

Przycisk::Przycisk(int in_x,int in_y,int in_w,int in_h)   {
        x=in_x;
        y=in_y;
        w=in_w;
        h=in_h;
        icon=ikonka1(w,h);
        klik=0;
        flaga=0;
    }

Przycisk::Przycisk(int in_x,int in_y,string in_tekst)   {
        tekst=in_tekst;
        w=tekst.size()*12+50;
        h=50;
        x=in_x-w/2;
        y=in_y-h/2;
        icon=przycisk_tekst(w,h,tekst);
        klik=0;
        flaga=0;
    }

void Przycisk :: draw_przycisk()  {
        al_get_mouse_state( & mysz );
        al_draw_bitmap(icon,x,y,0);
        if (x<=mysz.x && mysz.x<=x+w && y<=mysz.y && mysz.y<=y+h)   {
            flaga=1;

            if(mysz.buttons &1){
                cout<<"click";
                klik=1;
            }
        }
        else flaga=0;
    }

void Przycisk :: draw_przycisk(int in_x,int in_y)  {
        icon=kolo(w/2);
        w=20;
        h=20;
        x=in_x;
        y=in_y;
        al_get_mouse_state( & mysz );
        al_draw_bitmap(icon,x,y,0);
        if (x<=mysz.x && mysz.x<=x+w && y<=mysz.y && mysz.y<=y+h)   {
            flaga=1;

            if(mysz.buttons &1){
                cout<<"click"<<"    "<<endl;
                klik=1;
            }
        }
        else flaga=0;
    }

void rysuj_troj_sierp(int ax, int ay, int bx, int by, int cx, int cy, int st)   {
     int abx,aby,acx,acy,bcx,bcy;
     if (st>0)
        {
         bcx=int((bx+cx)/2);
         bcy=int((by+cy)/2);
         acx=int((ax+cx)/2);
         acy=int((ay+cy)/2);
         abx=int((ax+bx)/2);
         aby=int((ay+by)/2);
         al_draw_filled_triangle(abx,aby,bcx,bcy,acx,acy, al_map_rgb(255,255,255));
         rysuj_troj_sierp(ax,ay,abx,aby,acx,acy,st-1);
         rysuj_troj_sierp(abx,aby,bx,by,bcx,bcy,st-1);
         rysuj_troj_sierp(acx,acy,bcx,bcy,cx,cy,st-1);
        }
     else
        {
         al_rest(0.1); //opoznienie wyswietlenia
         al_flip_display();
        };
    }

void zmien_kursor(bool flaga)
{
    ALLEGRO_DISPLAY *okno =al_get_current_display();
    if(flaga)
    {
        ALLEGRO_BITMAP *pic2 = al_load_bitmap("kursor_lapka.png");
        ALLEGRO_MOUSE_CURSOR *kursor=al_create_mouse_cursor(pic2,10,0);
        al_set_mouse_cursor(okno,kursor);
    }
    else
    {
        ALLEGRO_BITMAP *pic1 = al_load_bitmap("kursor_niebieski.png");
        ALLEGRO_MOUSE_CURSOR *kursor_start=al_create_mouse_cursor(pic1,10,5);
        al_set_mouse_cursor(okno,kursor_start);
    }
}

void zapisz_wynik(int punkty)
{
    SYSTEMTIME data;
    GetSystemTime(&data);
    string wyniki="wyniki.txt";
    fstream plikout;
    plikout.open(wyniki.c_str(), ios::out | ios::app);
                plikout <<punkty
                        <<" "<<data.wDay
                        <<"-"<<data.wMonth
                        <<"-"<<data.wYear
                        <<"   "<<data.wHour+2
                        <<":"<<data.wMinute
                        <<endl;
    plikout.close();
}

Wyniki :: Wyniki(int in_k)
{
    k=in_k;
    wej="wyniki.txt";
    font=al_load_ttf_font("Tungsten-Bold.ttf", 20, 0);

    plikin.open(wej.c_str(), ios::in);
    if(!plikin.good())
    {
        cout<<"nie mozna wczytac wynikow";
    }
    else
    {
        int i = 0;
        while(!plikin.eof() && i<k)
        {
                plikin>>punkty[i];
                plikin>>data[i];
                plikin>>godzina[i];
                cout<<punkty[i]<<endl;
                cout<<data[i]<<endl;
                cout<<godzina[i]<<endl;
                i++;
        }

    }

    plikin.close();
}

void Wyniki :: wypisz_wyniki()
{
    int in_x=200;
    int in_y=20;
    al_draw_text(font,al_map_rgb(56,33,91),in_x,in_y,ALLEGRO_ALIGN_LEFT,"Nr.|Punkty|Data|Godzina");
    for(int i=0;i<k;i++)
    {
        al_draw_textf(font,al_map_rgb(56,33,91),in_x,in_y+(i+1)*20,ALLEGRO_ALIGN_LEFT," %i   |%s |%s |%s",i+1,punkty[i].c_str(),data[i].c_str(),godzina[i].c_str());
        cout<<punkty[i]<<" "<<data[i]<<"    "<<godzina[i]<<endl;
    }
}

