#include "Ranking.h"
#include <iostream>

using namespace std;

bool ranking
(
    ALLEGRO_EVENT_QUEUE *kolejka1,
    ALLEGRO_KEYBOARD_STATE klawiatura,
    ALLEGRO_FONT *font
)
{
    int frames;

    double fps, startTime, elapsedTime;
    startTime = elapsedTime = al_get_time();
    fps = 0;    frames = 0;

    ALLEGRO_EVENT event1;

    Wyniki r1(5);
    Przycisk p_menu(600,25,"MENU");

    cout <<"Ranking zaladowany"<<endl;
    zmien_kursor(0);
    bool dziala = true; while(dziala==true)
    {
        al_get_keyboard_state( & klawiatura);
        al_clear_to_color( al_map_rgb(255,255,0));
        fps = round((frames/elapsedTime)*10)/10;
        al_draw_textf(font,al_map_rgb(0,0,0),0,0,ALLEGRO_ALIGN_LEFT,"FPS: %g",fps); //FPS
        p_menu.draw_przycisk();

        r1.wypisz_wyniki();

        al_wait_for_event(kolejka1, &event1);

        if (al_key_down(&klawiatura, ALLEGRO_KEY_ESCAPE)  || event1.type == ALLEGRO_EVENT_DISPLAY_CLOSE)    dziala = false; //Wylaczanie programu
        if (event1.type == ALLEGRO_EVENT_TIMER )al_flip_display(); //Odswierzanie

        frames++;   elapsedTime = al_get_time() - startTime;    //FPS
    }
    return 1;
}
