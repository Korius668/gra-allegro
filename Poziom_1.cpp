#include "Poziom_1.h"
#include <iostream>

using namespace std;

bool poziom1
(
ALLEGRO_EVENT_QUEUE *kolejka1,
ALLEGRO_KEYBOARD_STATE klawiatura,
ALLEGRO_MOUSE_STATE mysz,
ALLEGRO_MOUSE_CURSOR *kursor,
ALLEGRO_FONT *font,
ALLEGRO_FONT *font2
)
{
    cout << "Poziom1 zaladowano";

    bool czy_na_przycisk;
    bool czy_klik;

    int frames;
    double fps, startTime, elapsedTime;
    startTime = elapsedTime = al_get_time();
    fps = 0;    frames = 0;
    int odliczanie=10;
    int punkty=0;
    int czas,czas2,czas3;
    bool start_odliczanie=0;
    int x=320,y=240;

        Przycisk celownik(0,0,20,20);
        Przycisk jeszcze_raz(320,50,"JESZCZE RAZ");
        Przycisk zapisz(320,150,"ZAPISZ");
        Przycisk nie_zapisuj(320,250,"WRÓĆ DO MENU");

        bool dziala = true; while(dziala==true)   {
            ALLEGRO_EVENT event1;

            al_get_keyboard_state( & klawiatura);
            al_clear_to_color( al_map_rgb((x+y)%200,x%255,y%255));
            if(odliczanie==0) {
                    if(nie_zapisuj.klik || jeszcze_raz.klik)czy_klik=true;
                    else   {
                    jeszcze_raz.draw_przycisk();
                    zapisz.draw_przycisk();
                    nie_zapisuj.draw_przycisk();
                    al_draw_textf(font2,al_map_rgb(255-((x+y)%200),255-(x%255),255-(y%255)),320,300,ALLEGRO_ALIGN_CENTER,"PUNKTY: %i",punkty);
                    }
            }
            else{
                if (celownik.klik) {
                    x=(int)(startTime*elapsedTime*fps*1000)%630;
                    y=(int)(startTime*elapsedTime*fps*100)%470;
                    punkty++;
                    celownik.klik=0;
                        if(!start_odliczanie)   {
                            start_odliczanie=1;
                            czas=al_get_time();
                            czas3=al_get_time()-czas;
                        }
                }

                if(start_odliczanie)  {
                    czas2= al_get_time()- czas;
                    odliczanie= odliczanie-(czas2-czas3);
                    czas3=czas2;
                }
            al_draw_textf(font2,al_map_rgb(255-((x+y)%200),255-(x%255),255-(y%255)),320,100,ALLEGRO_ALIGN_CENTER,"CZAS: %i",odliczanie);
            al_draw_textf(font2,al_map_rgb(255-((x+y)%200),255-(x%255),255-(y%255)),320,300,ALLEGRO_ALIGN_CENTER,"PUNKTY: %i",punkty);
            celownik.draw_przycisk(x,y);
            }

            if(celownik.flaga || zapisz.flaga || nie_zapisuj.flaga || jeszcze_raz.flaga)czy_na_przycisk=1;else czy_na_przycisk=0;   zmien_kursor(czy_na_przycisk);

                fps = round((frames/elapsedTime)*10)/10;
                al_draw_textf(font,al_map_rgb(0,0,0),0,0,ALLEGRO_ALIGN_LEFT,"FPS: %g",fps); //FPS

            al_wait_for_event(kolejka1, &event1);

        if (al_key_down(&klawiatura, ALLEGRO_KEY_ESCAPE)  || event1.type == ALLEGRO_EVENT_DISPLAY_CLOSE || czy_klik )    dziala = false; //Wylaczanie programu
        if (event1.type == ALLEGRO_EVENT_TIMER )al_flip_display(); //Odswierzanie

            frames++;   elapsedTime = al_get_time() - startTime;    //FPS

        if(jeszcze_raz.klik)    {
            return 0;
        }


        }
        if(zapisz.klik) {
                zapisz_wynik(punkty);
        }
        if(nie_zapisuj.klik) {
            return 1;
        }
}
