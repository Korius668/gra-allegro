#include "Menu.h"
#include "Poziom_1.h"
#include "Ranking.h"
using namespace std;

ALLEGRO_EVENT_QUEUE *kolejka1;
ALLEGRO_TIMER *timer;
ALLEGRO_DISPLAY *okno;
ALLEGRO_KEYBOARD_STATE klawiatura;
ALLEGRO_MOUSE_STATE mysz;
ALLEGRO_MOUSE_CURSOR *kursor;
ALLEGRO_FONT *font;
ALLEGRO_FONT *font2;

int menu()
{
    bool czy_na_przycisk;
    bool czy_klik; // Pokazuje czy kursor jest na przycisku

    al_init();  //Inicjowanie dodatkow
    al_init_primitives_addon();
    al_init_image_addon();
    al_init_font_addon();
    al_init_ttf_addon();

czy_na_przycisk=0; czy_klik=0;

kolejka1 = al_create_event_queue();
timer = al_create_timer( ALLEGRO_BPS_TO_SECS(60) );
    al_start_timer(timer);

    al_install_keyboard();  //Inicjowanie myszki i klawiatury
    al_install_mouse();

ALLEGRO_BITMAP *ikonka=ikonka1();;1
//ALLEGRO_BITMAP *ts=troj_sierp(640,480);   //Inicjowanie obrazkow

    ALLEGRO_MONITOR_INFO info;  //Pobieranie wartosci monitora
    int c = al_get_num_video_adapters();
    for (int i = 0; i < c; ++i) al_get_monitor_info(i, &info);

    int w,h;    w= 640; h= 480; //Wielkosc okna
    okno = al_create_display(w,h); //Inicjowanie okna
        //Parametry okna
        al_set_display_icon( okno, ikonka );  //Ikonka
        al_set_window_title( okno,"SuperGra" );  //Tytul
        al_set_new_display_flags( ALLEGRO_RESIZABLE );
        al_set_window_position( okno, info.x2/2-w/2, info.y2/2-h/2 );  //Pozycja

font = al_load_ttf_font("Tungsten-Bold.ttf", 20, 0);
font2 = al_load_ttf_font("Tungsten-Bold.ttf", 100, 0);

//Kursor
   //kursor=al_create_mouse_cursor(ikonka,0,0);
   // al_set_mouse_cursor(okno,kursor);
//EVENT

    al_register_event_source(kolejka1, al_get_display_event_source(okno));
    al_register_event_source(kolejka1, al_get_timer_event_source(timer));
    Przycisk p_start(320,140,"START");
    Przycisk p_ranking(320,240,"RANKING");
    Przycisk p_exit(320,340,"EXIT");


//Czêstoœæ odœwie¿ania
int frames;
double fps, startTime, elapsedTime;
startTime = elapsedTime = al_get_time();
fps = 0;    frames = 0; //fps - klatki na sekuned; frames- klatki

bool dziala = true; while(dziala==true)   {
        al_clear_to_color( al_map_rgb( 200, 255, 0 ) );

        ALLEGRO_EVENT event1;
        al_get_keyboard_state( & klawiatura);


        p_start.draw_przycisk();
        p_ranking.draw_przycisk();
        p_exit.draw_przycisk();
        if (p_start.flaga || p_ranking.flaga || p_exit.flaga) czy_na_przycisk=1;    else czy_na_przycisk=0;
        zmien_kursor(czy_na_przycisk);

        //else zmien_kursor(0);

        if(p_start.klik)
        {
            p_start.klik=0;
            if(!poziom1(kolejka1,klawiatura,mysz,kursor,font,font2)) p_start.klik=1;

            startTime = elapsedTime = al_get_time();
            fps = 0;    frames = 0;
        }

        if(p_ranking.klik)
        {
            if(!ranking(kolejka1,klawiatura,font))    cout<<"Rankingu tu nie ma"<<endl;
            p_ranking.klik=0;
        }

        fps = round((frames/elapsedTime)*10)/10;

        al_draw_textf(font,al_map_rgb(0,0,0),0,0,ALLEGRO_ALIGN_LEFT,"FPS: %g",fps); //FPS

        al_wait_for_event(kolejka1, &event1);
    if (p_start.klik || p_ranking.klik || p_exit.klik) czy_klik=1;    else czy_klik=0;
    if (al_key_down(&klawiatura, ALLEGRO_KEY_ESCAPE)  || event1.type == ALLEGRO_EVENT_DISPLAY_CLOSE || p_exit.klik )    dziala = false; //Wylaczanie programu
    if (event1.type == ALLEGRO_EVENT_TIMER)al_flip_display(); //Odswierzanie

        frames++;   elapsedTime = al_get_time() - startTime;    //FPS
}


al_uninstall_keyboard();
al_uninstall_mouse();
al_destroy_display(okno);
al_destroy_timer(timer);
al_shutdown_primitives_addon();
al_shutdown_image_addon();
al_shutdown_font_addon();
al_shutdown_ttf_addon();
return 0;
}
