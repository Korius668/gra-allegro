#pragma once
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <bits/stdc++.h>
#include <windows.h>
#include <winbase.h>
#include <ctime>

using namespace std;

ALLEGRO_BITMAP *ikonka1(int w=10,int h=10);
ALLEGRO_BITMAP *troj_sierp(int w=100,int h=100);
ALLEGRO_BITMAP *przycisk_tekst(int w=100, int h=100, string tekst="");
ALLEGRO_BITMAP *kolo(int w,int h);
////////////////////////////////////////////////////
class Przycisk{

    ALLEGRO_MOUSE_STATE mysz;
    ALLEGRO_BITMAP *icon;
    string tekst;
public:
    int x;
    int y;
    int w;
    int h;
    bool flaga;
    bool klik;
    Przycisk(int in_x=0,int in_y=0,string in_tekst="");
    Przycisk(int in_x=0,int in_y=0,int in_w=50,int in_h=20);

    void draw_przycisk();
    void draw_przycisk(int x,int y);
    };
//////////////////////////////////////////////////////
void rysuj_troj_sierp(int ax, int ay, int bx, int by, int cx, int cy, int st);
void zmien_kursor(bool flaga);
void zapisz_wynik(int punkty);
//////////////////////////////////////
class Wyniki
{
    int k;
    string punkty[5], data[5], godzina[5], wej;
    fstream plikin;
    ALLEGRO_FONT *font;
public:
    Wyniki(int in_k=5);
    void wypisz_wyniki();
};
